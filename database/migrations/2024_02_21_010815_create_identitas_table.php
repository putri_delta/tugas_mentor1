<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('identitas', function (Blueprint $table) {
            $table->id('id');
            $table->string('Nama', 50);
            $table->integer('NIM', 20);
            $table->enum('Jenis_Kelamin', ['Laki-laki', 'Perempuan',]);
            $table->string('Prodi', 50);
            $table->string('Fakultas', 50);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('identitas');
    }
};
