<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_diris', function (Blueprint $table) {
            $table->id();
            $table->string('Nama', 50);
            $table->bigInteger('NIM', false, true);
            $table->enum('Jenis_Kelamin', ['Laki-laki', 'Perempuan',]);
            $table->string('Prodi', 50);
            $table->string('Fakultas', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_diris');
    }
};
