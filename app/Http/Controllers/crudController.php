<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\data_diri;
class crudController extends Controller
{
    public function index()
    {
        $datadiri = data_diri::all(); 
        return view('data', compact('datadiri'));
    }
    
    public function create()
    {
        return view('tambahdata');
    }
    
    //CRUD 
    //CREATE
    public function store(request $request){
        {
            $request->validate([
                'Nama' => 'required',
                'NIM' => 'required',
                'Jenis_Kelamin' => 'required',
                'Prodi' => 'required',
                'Fakultas' => 'required',
            ]);

            data_diri::create($request->all());
            return redirect()->route('data.index')->with('success','Product created successfully.');
            }
    }
     //READ
     public function show(data_diri $datadiri){
        return view('data',compact('datadiri'));
    }

    public function edit($id){
    $datadiri = data_diri::find($id);
    return view('edit', compact('datadiri'));
    }


    //UPDATE
    public function update($id, Request $request)
    {
        $request->validate([
            'Nama' => 'required',
            'NIM' => 'required',
            'Jenis_Kelamin' => 'required',
            'Prodi' => 'required',
            'Fakultas' => 'required',
        ]);

        $datadiri = data_diri::find($id);

        if(!$datadiri) {
            return redirect()->route('data.index')->with('error', 'Data not found');
        }

        $datadiri->update($request->all());

        return redirect()->route('data.index')->with('success', 'Data updated successfully');
    }
    //delete
    public function destroy($id)
    {
        $datadiri = data_diri::find($id);
        $datadiri->delete();
        return redirect()->route('data.index')->with('success','Product deleted successfully');
    }

}
