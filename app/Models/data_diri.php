<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_diri extends Model
{
    use HasFactory;
    protected $fillable = [
        'Nama',
        'NIM',
        'Jenis_Kelamin',
        'Prodi',
        'Fakultas',
    ];
}
