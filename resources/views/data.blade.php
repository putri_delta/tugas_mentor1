<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Diri</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container" style="padding-top: 50px;" id="produklayanan">
    <h2 class="display-6 pb-3">Data Diri</h2>
    <a href="{{ route('data.create') }}" class="btn btn-primary mb-2">Tambah Data</a>
    <table class="table table-bordered">
        <thead>
            <tr class="text-center">
                <th scope="col">Nama</th>
                <th scope="col">NIM</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Prodi</th>
                <th scope="col">Fakultas</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($datadiri as $s)
            <tr>
                <td>{{ $s->Nama }}</td>
                <td>{{ $s->NIM }}</td>
                <td>{{ $s->Jenis_Kelamin }}</td>
                <td>{{ $s->Prodi }}</td>
                <td>{{ $s->Fakultas }}</td>
                <td class="text-center">
                    <a href="/data/{{ $s->id }}/edit" class="btn btn-sm btn-primary">edit</a>
                    <form action="/data/{{ $s->id }}" method="POST" style="display:inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Yakin ingin menghapus item');" class="btn btn-sm btn-danger"></i>delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6" class="text-center">
                    <div class="alert alert-danger">
                        Data Data Diri belum tersedia.
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
