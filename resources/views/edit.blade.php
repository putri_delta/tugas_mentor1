<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Data</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('data.update', $datadiri->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="Nama">Nama</label>
                                <input type="text" class="form-control" id="Nama" name="Nama" value="{{ $datadiri->Nama }}">
                            </div>

                            <div class="form-group">
                                <label for="NIM">NIM</label>
                                <input type="text" class="form-control" id="NIM" name="NIM" value="{{ $datadiri->NIM }}">
                            </div>

                            <div class="form-group">
                                <label for="Jenis_Kelamin">Jenis Kelamin</label>
                                <select class="form-control" id="Jenis_Kelamin" name="Jenis_Kelamin">
                                    <option value="Laki-laki" {{ $datadiri->Jenis_Kelamin == 'Laki-laki' ? 'selected' : '' }}>Laki-laki</option>
                                    <option value="Perempuan" {{ $datadiri->Jenis_Kelamin == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="Prodi">Prodi</label>
                                <input type="text" class="form-control" id="Prodi" name="Prodi" value="{{ $datadiri->Prodi }}">
                            </div>

                            <div class="form-group">
                                <label for="Fakultas">Fakultas</label>
                                <input type="text" class="form-control" id="Fakultas" name="Fakultas" value="{{ $datadiri->Fakultas }}">
                            </div>

         

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
